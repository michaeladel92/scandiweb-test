  <!-- product-list-start -->
 <main class="products">
    <div class="heading">
      <h1 class="title upper-case">product list</h1>
      <button id="mass-delete" class="upper-case teko-font danger">mass delete</button>
    </div>
    <div class="container" id="product-list">
    <?php
    $obj = new Vendor\Product(new Vendor\DatabaseConnect);
    echo $obj->resultProduct();
    ?>
    </div>
  </main>
  <!-- product-list-end -->