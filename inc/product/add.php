<?php
require_once( __DIR__.'/../../inc/config.php');
use Vendor\Product as ProductObj;
use Vendor\Message as MessageObj;

// initiate
$messageObj = new MessageObj; //static Class

/**
 * ================================
 *  PRODUCT FORM
 * ================================
 * REQUEST:POST
 * KEY:[form] set is a MUST = used for differentiate between Forms(multi forms)
 * TOKEN:each form has it's own token
 */
if( 
  $_SERVER['REQUEST_METHOD'] == "POST" 
  && 
  isset($_POST['form'])
  &&
  isset($_SESSION['token'])
  ){
    //Add Product Process
    if($_POST['form'] === 'add-product'){
       
        // check Token
        if($_SESSION['token'] === $_POST['token']){
          
          // initiate
          $productObj  = new ProductObj(new Vendor\DatabaseConnect);
          $productObj->processingPostProduct($_POST);
 
        
        }else{
          $messageObj::setMsg("Token Expired!");
          $messageObj::$notifications[] = ["other" => $messageObj::getMsg()];
          exit(json_encode($messageObj::$notifications));
        
        }
    }else{

      $messageObj::setMsg("opps, something went wrong 1!");
      $messageObj::$notifications[] = ["other" => $messageObj::getMsg()];
      exit(json_encode($messageObj::$notifications));
    
    }


  }else{

    // For security and not reading the file name
    $page = new Vendor\page;
    $page->redirect('../../');

  }