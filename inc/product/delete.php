<?php

require_once( __DIR__.'/../../inc/config.php');
use Vendor\Product as ProductObj;


// initiate
$ProductObj = new ProductObj(new Vendor\DatabaseConnect);

/**
 * ================================
 *  PRODUCT FORM
 * ================================
 * REQUEST:POST
 * KEY:[form] set is a MUST = used for differentiate between Forms(multi forms)
 * TOKEN:each form has it's own token
 */

if( 
  $_SERVER['REQUEST_METHOD'] == "POST" 
  && 
  isset($_POST['form'])
  ){

    if(
      $_POST['form'] == 'mass-delete-product'
      &&
      isset($_POST['selectedList'])
      ){

        $ProductObj->processingMassDelete($_POST);
   
      
      }

  }else{
       // For security and not reading the file name
       $page = new Vendor\page;
       $page->redirect('../../');
  }

