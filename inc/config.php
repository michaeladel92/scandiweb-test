<?php
$dirname = dirname(__DIR__);
require_once($dirname .'/autoload.php'); 

use Vendor\Session as SessionObj;
use Vendor\Page as PageObj;

// start Seasion
$session = new SessionObj;
$page = new PageObj;


/*
 *Get url path
 */ 
$http = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https://' : 'http://'); 

$url = $_SERVER['REQUEST_URI']; 
$parts = explode('/',$url);
$dir = $_SERVER['SERVER_NAME'];
for ($i = 0; $i < count($parts) - 1; $i++) {
 $dir .= $parts[$i] . "/";
}

$mainUrl = $http . $dir;

/*
 * Database connect constant
 */
const DB_HOST = 'localhost';
const DB_USER = 'root';
const DB_PASS = '';
const DB_NAME = 'scandiweb';






 