<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- SEO -->
  <meta name="description" content="description content here!">
  <meta name="keywords" content="php, mysql, html, css, javaScript,scandiweb test">
  <meta name="robots" content="index, follow" />

  <!-- fontawsome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css"
    integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
  <!-- Style - CSS -->
  <link rel="stylesheet" href="css/style.css">
  <?php  if($session->url_check("/scandiweb/",'Home')): ?>
  <link rel="stylesheet" href="css/product-list.css">
  <?php endif; ?>  
  <?php if($session->url_check("/scandiweb/add-product",'Add Product')): ?>  
  <link rel="stylesheet" href="css/product-add.css">
  <?php endif; ?>  
  <title><?php echo $session->get_title_page();?></title>
</head>

<body>