class XmlFetch {
  post(url, data) {
    return new Promise((resolve, reject) => {
      fetch(url, {
        method: "POST",
        body: data
      })
        .then(res => res.text())
        .then(succ => resolve(succ))
        .catch(error => reject(error))
    })
  }
}