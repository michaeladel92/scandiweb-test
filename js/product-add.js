/* 
=================================
Global
=================================
*/
const
  productTypeSelect = document.getElementById('productType'),
  // the container to append the html inputs
  productOptions = document.getElementById('productOption'),
  inputPrice = document.getElementById('price'),
  productForm = document.getElementById('product_form'),
  saveBtn = document.querySelector('[name = "submit"]'),
  notificationKeys = ['productType', 'sku', 'name', 'price', 'size', 'weight', 'height', 'width', 'length', 'other']

/* 
=================================
Events
=================================
*/

// get product type form additional inputs
productTypeSelect.addEventListener('change', additionalFormInputs)

inputPrice.addEventListener('input', moneyFormatInput)


//Form Submit
productForm.addEventListener('submit', e => {
  e.preventDefault();
  // disable btn when in process
  saveBtn.setAttribute('disabled', 'disabled');


  const data = new FormData(productForm)
  data.append('form', 'add-product')

  const xml = new XmlFetch()

  xml.post('add-api', data)
    .then(result => {
      console.log(result)
      // Rmv all Error Msg
      const allValidateEl = document.querySelectorAll('.validate')
      Array.from(allValidateEl).forEach(item => {
        item.innerHTML = '&nbsp;'//for fix design 
        item.classList.remove('show')
        item.previousElementSibling.classList.remove('error')
      })

      const messages = JSON.parse(result)

      messages.forEach(item => {
        // [Google Searched]
        for (key in item) {

          if (notificationKeys.includes(key)) {

            const el = document.getElementById(key)

            if (el.nextElementSibling.classList.contains('validate')) {

              el.classList.add('error')
              el.nextElementSibling.innerHTML = item[key]
              el.nextElementSibling.classList.add('show')

            }
            // enable btn
            saveBtn.removeAttribute('disabled')

          } else if (key === 'redirect') {

            // success and reload to main
            window.location.href = './'

          }

        }
      })

    })
    .catch(error => console.log(error))
})
/* 
=================================
Functions
=================================
*/


function formSize() {
  const formStyle = `     
  <!-- size -->
  <div class="form-controller">
    <label for="size" class=" Oswald-font">size(mb)</label>
    <input type="text" id="size" name="size" placeholder="product size (mb)">
    <small class="validate">&nbsp;</small>
  </div>
  ${formNote('Please, provide size!')}
  `
  return formStyle
}

function formWeight() {
  const formStyle = `     
  <!-- weight -->
  <div class="form-controller">
    <label for="weight" class=" Oswald-font">weight(kg)</label>
    <input type="text" id="weight" name="weight" placeholder="product weight (kg)">
    <small class="validate">&nbsp;</small>
  </div>
  ${formNote('Please, provide weight!')}
  `
  return formStyle
}

function formDimention() {
  const formStyle = `     
  <!-- height -->
  <div class="form-controller">
    <label for="height" class=" Oswald-font">height(cm)</label>
    <input type="text" id="height" name="height" placeholder="product height (cm)">
    <small class="validate">&nbsp;</small>
  </div>
  <!-- width -->
  <div class="form-controller">
    <label for="width" class=" Oswald-font">width(cm)</label>
    <input type="text" id="width" name="width" placeholder="product width (cm)">
    <small class="validate">&nbsp;</small>
  </div>
  <!-- length -->
  <div class="form-controller">
    <label for="length" class=" Oswald-font">length(cm)</label>
    <input type="text" id="length" name="length" placeholder="product length (cm)">
    <small class="validate">&nbsp;</small>
  </div>
  ${formNote('Please, provide dimensions!')}
  `
  return formStyle
}

function formNote(msg) {
  const formStyle = `   
  <!-- default notification -->
  <div class="form-controller notice">
    <div class="default-notification">
      <div class="image-container">
        <img src="img/lamp.png" alt="lamp note">
      </div>
      <p>${msg}</p>
    </div>
  </div>
  `
  return formStyle
}

function additionalFormInputs() {
  const selected = this.value.toLowerCase()
  const allVal = {
    'dvd': formSize(),
    'furniture': formDimention(),
    'book': formWeight()
  }
  productOptions.innerHTML = ''
  if (selected in allVal) {
    productOptions.innerHTML = allVal[selected]
  }

  //to validate number only inputs
  typeInputsDeclaration()

}

function typeInputsDeclaration() {
  const
    inputSize = document.getElementById('size'),
    inputSeight = document.getElementById('weight'),
    inputHeight = document.getElementById('height'),
    inputWidth = document.getElementById('width'),
    inputLength = document.getElementById('length')

  const numberOnlyArr = [inputSize, inputSeight, inputHeight, inputWidth, inputLength]
  // validate all input numbers
  numberOnlyArr.forEach(item => {
    if (item !== null) {
      item.addEventListener('input', acceptNumberAndDecimalOnly)
    }
  })
}

function acceptNumberAndDecimalOnly() {
  const val = validDigitsWithDicemal(this.value)
  this.value = val

}

// filter input into numbers [Google Searched]
function validDigits(n) {
  return n.replace(/[^\d]+/g, '');
}

function validDigitsWithDicemal(n) {
  let val = n.replace(/[^\d.]+/g, '');
  if (val.split('.').length > 2) {
    val = val.replace(/\.+$/, "");
  }
  return val
}

function moneyFormatInput() {

  const val = validDigits(this.value)
  // format input number with Comma's [Google Searched]
  const formatedVal = moneyFormatShow(val)

  if (formatedVal == 'NaN') {
    this.value = 0
  } else {
    this.value = formatedVal
  }

}

function moneyFormatShow(number) {
  return parseFloat(number).toLocaleString('en-US', {
    style: 'decimal',
    maximumFractionDigits: 0,
    minimumFractionDigits: 0
  })
}

