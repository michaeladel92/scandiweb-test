/* 
=================================
Global
=================================
*/
const
  productList = document.getElementById('product-list'),
  deleteCheckbox = document.querySelectorAll('.delete-checkbox'),
  massDeleteBtn = document.getElementById('mass-delete')

let selectedList = []

/* 
=================================
Events
=================================
*/
// return selected id of products in arr 
productList.addEventListener('click', getSelectedArr)

// delete selected array if exist
massDeleteBtn.addEventListener('click', deleteSelectedProduct)

/* 
=================================
Functions
=================================
*/

function getSelectedArr(e) {
  if (e.target.classList.contains('delete-checkbox')) {
    selectedList = [] //to get the new values
    deleteCheckbox.forEach(item => {
      if (item.checked) {
        selectedList.push(item.value)
      }
    })
  }

  return selectedList //return updated value
}

// ajax Call
function deleteSelectedProduct(e) {
  e.preventDefault()
  if (selectedList.length > 0) {


    const data = new FormData()
    data.append('selectedList', JSON.stringify(selectedList))
    data.append('form', 'mass-delete-product')

    const xml = new XmlFetch()
    xml.post('delete-api', data)
      .then(result => {
        const msg = JSON.parse(result)
        if (msg[0].redirect == 'deleted successfully!') {
          // success and reload to main
          window.location.href = './'
        }
      })
      .catch(error => console.log(error))


  } else {
    alert('Please Select Product you want to delete')
  }
}