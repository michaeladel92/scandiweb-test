<?php

namespace Vendor;

class Page{

// redirect to non php extension
public function redirectToNonPHPExt(){
  $filenameUrl = strtolower(basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']));
if($filenameUrl === 'index.php' || $filenameUrl === 'index'){$this->redirect("./");}
elseif($filenameUrl === 'add-product.php'){$this->redirect("./add-product");}
}

//Redirect
public function redirect($dir){
  ob_start();
  header('Location:'.$dir);
}

}