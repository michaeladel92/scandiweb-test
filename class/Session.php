<?php

namespace Vendor;

class Session {


  function __construct(){
    ob_start();
    session_start();

  }

  // Generate auto token [Google Searched]
  private function token_generator(){
    $token = openssl_random_pseudo_bytes(16);
    $token = bin2hex($token);
    return $token;
  }

  public function set_token(){
    $_SESSION['token'] = $this->token_generator();
    return $_SESSION['token'];
  }
  /*
   *Show Certian links depend on url 
   */
  public function url_check($dir,$title = ''){
    
    if(strtolower(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH)) == $dir){
    
      $this->set_title_page($title);
      return true;
    
    }else{return false;}
    
  }

  private function set_title_page($title){
    $_SESSION['title_page'] = $title;
  }
  
  public function get_title_page(){
    if(isset($_SESSION['title_page'])){
      return $_SESSION['title_page'];
      unset($_SESSION['title_page']);
    }
    else{ return "Scandiweb";}
  }

  function __destruct(){
    ob_end_flush();
  }
}