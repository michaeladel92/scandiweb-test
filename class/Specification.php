<?php
namespace Vendor;

abstract  class Specification{
    /**
     * NOTE::
     * $table must follow lowercase pattern && same as db table names
     * properties name follow as db table names
     * */  
    private 
            $table = ['size','weight','width','height','length'],
            $size, $weight, $width, $height, $length, $type_id,$keys = "`type_id`,",$vals = [];
    protected $db;

    public function __construct(DatabaseConnect $db){
      // echo "start specification";
      return $this->db = $db; 
    }        
    

    public function typeArray(){
     return $this->db->query(" * " , "types");
    }
    /*
     * Filter & Validate Specifications
     * @set Message Notifications
     * @return True | False
     */ 
    
    public function validateProductType($productType,array $typesArry = []){
      $validate = new Validation;
      //product type empty
      if(!$validate->validate($productType,'empty')){
      
        Message::setMsg("Product Type is required!");
        Message::$notifications[] = ["productType" => Message::getMsg()];
        return false;
      
      }
      
      //FixBig: need to make lowercase in array_keys
      $productType = $validate->filter($productType);

      foreach($this->typeArray() as $key => $val){
        
        $type[$val['id']] = strtolower($val['type']);
      
      }
      
      if(!$validate->validate($validate->filter($productType),'inArray',null,null,$type)){
      
        Message::setMsg("Oops, an error accured, \"{$productType}\" not found in our database, please try again!");
        Message::$notifications[] = ["other" => Message::getMsg()];
      
      }else{

          $this->type_id = array_keys($type, $productType)[0];// set id
           
          $table =  $this->table;

          
          for($i=0; $i <= count($table) -1; $i++ ){
             //check if the given array exist in our $table array
            if(array_key_exists($table[$i], $typesArry)){
              //make sure that the given property is exist
              if(property_exists('Vendor\Specification',$table[$i])){
                //set the property to the given value | filter the value
               $this->{$table[$i]} =  $validate->filter($typesArry[$table[$i]],'dot');
               
              }  
            }
          }
         //validate the properties that set
         foreach($this->table as $property){
          
          if(isset($this->{$property})){
            $this->keys .= "`{$property}`,";
            $this->vals[] = $this->{$property};
            //empty    
            if(!$validate->validate($this->{$property},'empty')){
              
              Message::setMsg("{$property} field is required!");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];
              
            }
            //numeric
            elseif(!$validate->validate($this->{$property},'numeric')){
            
              Message::setMsg("{$property} must be  numeric");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];
              
            }
            //first char cannot be zero
            elseif(!$validate->validate($this->{$property},'firstChar')){

              Message::setMsg("{$property} cannot start with zero");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];

            }
            // max 10 char
            elseif(!$validate->validate($this->{$property},'max',1,11)){
              
              Message::setMsg("{$property} max 10 char");
              Message::$notifications[] = ["{$property}" => Message::getMsg()];

            } 
          }
        }
          
          if(count(Message::$notifications) > 0){return false;}
          else{return true;}
      }
    
    }
    /*
     * INSERT To DataBase Process:
     * @RETURN new Row Id
     */
    public function insertSpecificationProcess(){
      
      array_unshift($this->vals,$this->type_id);
      $this->db->insert('specifications',$this->keys, $this->vals);
      if($this->db->execute()){
        
        return $this->db->lastId();
      
      }else{ return false;}
    }
  
    function __destruct(){
      // echo "end Specification";
      $this->size = null;
      $this->weight = null;
      $this->width = null;
      $this->height = null;
      $this->length = null;
      $this->type_id = null;
      $this->keys = "`type_id`,";
      $this->vals = [];
     
    }

}