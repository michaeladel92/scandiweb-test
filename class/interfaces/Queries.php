<?php
namespace Vendor\interfaces;

interface Queries{

    public function conn();
    public function query($column , $table , $other = null);
    public function queryCount($column , $table , $other = null);
    public function insert($table,$column,array $valueArr);
    public function delete($table, $other);
    public function execute();
    public function rowCount();
    public function fetch();
    public function lastId();
    public function bindParam(array $arrKey,array $arrVal);
    public function bindParamQuestions(int $num,array $arrVal);

}